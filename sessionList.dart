import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class SessionList extends StatefulWidget {
  const SessionList({Key? key}) : super(key: key);

  @override
  State<SessionList> createState() => _SessionListState();
}

class _SessionListState extends State<SessionList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("sessions").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _namefieldcontroller = TextEditingController();
    TextEditingController _universityfieldcontroller = TextEditingController();
    TextEditingController _locationfieldcontroller = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("sessions")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    } //end _delete

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("sessions");
      _namefieldcontroller.text = data["Name"];
      _universityfieldcontroller.text = data["University"];
      _locationfieldcontroller.text = data["Location"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _namefieldcontroller,
                  ),
                  TextField(
                    controller: _universityfieldcontroller,
                  ),
                  TextField(
                    controller: _locationfieldcontroller,
                  ),
                  TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "Name": _namefieldcontroller.text,
                          "University": _universityfieldcontroller.text,
                          "Location": _locationfieldcontroller.text,
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    } //end _update

    return StreamBuilder(
        stream: _mySessions,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return Text("Something went wrong");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          }
          if (snapshot.hasData) {
            return Row(
              children: [
                Expanded(
                    child: SizedBox(
                        height: (MediaQuery.of(context).size.height),
                        width: (MediaQuery.of(context).size.width),
                        child: ListView(
                          children: snapshot.data!.docs
                              .map((DocumentSnapshot documentSnapshot) {
                            Map<String, dynamic> data = documentSnapshot.data()!
                                as Map<String, dynamic>;
                            return Column(
                              children: [
                                Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        title: Text(data['Name']),
                                        subtitle: Text(data["Location"]),
                                      ),
                                      ButtonTheme(
                                        child: ButtonBar(
                                          children: [
                                            OutlineButton.icon(
                                              onPressed: () {
                                                _update(data);
                                              },
                                              icon: Icon(Icons.edit),
                                              label: Text("Update"),
                                            ),
                                            OutlineButton.icon(
                                              onPressed: () {
                                                _delete(data["doc_id"]);
                                              },
                                              icon: Icon(Icons.remove),
                                              label: Text("Delete"),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            );
                          }).toList(),
                        )))
              ],
            );
          } else {
            return (Text("No data"));
          }
        });
  }
}
