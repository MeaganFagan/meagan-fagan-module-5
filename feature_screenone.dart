import 'dart:developer';
import 'main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/home_page.dart';
import 'home_page.dart';
import 'sessionList.dart';

class feature_screenone extends StatelessWidget {
  TextEditingController nameController = TextEditingController();
  TextEditingController universityController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  Future gofeature_screenone() {
    final pname = nameController.text;
    final university = universityController.text;
    final location = locationController.text;
    final ref = FirebaseFirestore.instance.collection("sessions").doc();

    return ref
        .set({
          "Name": pname,
          "University": university,
          "Location": location,
          "doc_id": ref.id,
        })
        .then((value) => log("Collection Added"))
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('f1_Book Session'),
      ),
      body: Container(
          margin: EdgeInsets.all(16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              //_header(context),
              _inputfield(context),
              // _backtohome(context),
            ],
          )),
    );
  } //end widget build

  /*_header(context) {
    return Column(
      children: [
        Text(
          "book session",
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
        Text("Enter details below")
      ],
    );
  } */

  _inputfield(context) {
    return Column(children: [
      Column(
        children: [
          TextField(
            controller: nameController,
            decoration: InputDecoration(
                hintText: "Name",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none),
                fillColor: Theme.of(context).primaryColor.withOpacity(0.1),
                filled: true,
                prefixIcon: Icon(Icons.person)),
          ),
          SizedBox(height: 8),
          TextField(
            controller: universityController,
            decoration: InputDecoration(
                hintText: "university",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none),
                fillColor: Theme.of(context).primaryColor.withOpacity(0.1),
                filled: true,
                prefixIcon: Icon(Icons.person)),
          ),
          SizedBox(height: 8),
          TextField(
            controller: locationController,
            decoration: InputDecoration(
                hintText: "location",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide.none),
                fillColor: Theme.of(context).primaryColor.withOpacity(0.1),
                filled: true,
                prefixIcon: Icon(Icons.person)),
          ),
          SizedBox(height: 8),
          ElevatedButton(
            onPressed: () {
              gofeature_screenone();
            },
            child: Text(
              "Book Session",
              style: TextStyle(fontSize: 15),
            ),
          )
        ],
      ),
      SessionList()
    ]);
  } //end input field

 /* _backtohome(context) {
    return TextButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return home_page();
              },
            ),
          );
        },
        child: Text("Return to home page")); 
  }*/
}
