import 'package:flutter/material.dart';
import 'login_page.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'feature_screenone.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(apiKey: "AIzaSyBR6s0Gw7Z89Od8P8lk7_k74avLLETLdzg",
    authDomain: "mtnappm5.firebaseapp.com",
    projectId: "mtnappm5",
    storageBucket: "mtnappm5.appspot.com",
    messagingSenderId: "627689537475",
    appId: "1:627689537475:web:c9c412a458b53601084776")
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        //home: login_page(),
        theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.deepPurpleAccent,
          scaffoldBackgroundColor: Colors.grey,
        ),
        home: AnimatedSplashScreen(
          splash: Icon(
            Icons.attractions_sharp,
            size: 200,
          ),
          nextScreen: login_page(),
          splashTransition: SplashTransition.rotationTransition,
          duration: 3000,
          backgroundColor: Colors.purple,
        ));
  }
}
